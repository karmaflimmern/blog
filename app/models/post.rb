class Post < ActiveRecord::Base
	has_many :comments, dependent: :destroy
	validates_presence of :title
	validates_presence of :body

end
